# frozen_string_literal: true

require './lib/board.rb'

describe Board do
  subject(:board) { Board.new }

  describe '.board is initialized' do
    it 'has 8 rows' do
      expect(board.board.length).to eq(8)
    end
    it 'has 8 columns' do
      expect(board.board[0].length).to eq(8)
    end
    it 'has 32 pieces' do
      expect(board.board.flatten.count { |x| x != 0 }).to eq(32)
    end
    it 'has 16 white pieces' do
      expect(board.board.flatten.count { |x| x > 0 }).to eq(16)
    end
    it 'has 16 black pieces' do
      expect(board.board.flatten.count { |x| x < 0 }).to eq(16)
    end
    it 'has pieces in the correct starting positions' do
      expect(board.board[0]).to eq([4, 2, 3, 5, 6, 3, 2, 4])
      expect(board.board[1]).to eq([1, 1, 1, 1, 1, 1, 1, 1])
      expect(board.board[6]).to eq([-1, -1, -1, -1, -1, -1, -1, -1])
      expect(board.board[7]).to eq([-4, -2, -3, -5, -6, -3, -2, -4])
    end
  end

  describe '.king_in_check?' do

    context 'when white king' do
      context 'in check' do
        it 'returns true' do
          # fool's mate
          board.move_piece(Coordinate.new(5, 1), Coordinate.new(5, 2))
          board.move_piece(Coordinate.new(4, 6), Coordinate.new(4, 4))
          board.move_piece(Coordinate.new(6, 1), Coordinate.new(6, 3))
          board.move_piece(Coordinate.new(3, 7), Coordinate.new(7, 3))
          expect(board.king_in_check?).to be true
        end
      end
      context 'not in check' do
        it 'returns false' do
          expect(board.king_in_check?).to be false
        end
      end
    end

    context 'when black king' do
      context 'in check' do
        it 'returns true' do
          # fool's mate
          # random move so white can start sequence
          board.move_piece(Coordinate.new(0, 1), Coordinate.new(0, 2)) 

          board.move_piece(Coordinate.new(5, 6), Coordinate.new(5, 5))
          board.move_piece(Coordinate.new(4, 1), Coordinate.new(4, 3))
          board.move_piece(Coordinate.new(6, 6), Coordinate.new(6, 4))
          board.move_piece(Coordinate.new(3, 0), Coordinate.new(7, 4))
          expect(board.king_in_check?).to be true
        end
      end
      context 'not in check' do
        it 'returns false' do
          expect(board.king_in_check?).to be false
        end
      end
    end
  end

  describe '.pawn_ready_for_promotion?' do
    context 'when a pawn is ready for promotion' do
      context 'and its a white pawn' do
        it 'returns true' do
          # move white pawn to end
          board.move_piece(Coordinate.new(0, 1), Coordinate.new(0, 3))
          board.move_piece(Coordinate.new(7, 6), Coordinate.new(7, 5))
          board.move_piece(Coordinate.new(0, 3), Coordinate.new(0, 4))
          board.move_piece(Coordinate.new(7, 5), Coordinate.new(7, 4))
          board.move_piece(Coordinate.new(0, 4), Coordinate.new(0, 5))
          board.move_piece(Coordinate.new(7, 4), Coordinate.new(7, 3))
          board.move_piece(Coordinate.new(0, 5), Coordinate.new(1, 6))
          board.move_piece(Coordinate.new(7, 3), Coordinate.new(7, 2))
          board.move_piece(Coordinate.new(1, 6), Coordinate.new(2, 7))
          expect(board.pawn_ready_for_promotion?[0]).to be true
        end
      end

      context 'and its a black pawn' do
        it 'returns true' do
          # move black pawn to end
          # move white pawn to end
          board.move_piece(Coordinate.new(0, 1), Coordinate.new(0, 2))
          board.move_piece(Coordinate.new(7, 6), Coordinate.new(7, 4))
          board.move_piece(Coordinate.new(0, 2), Coordinate.new(0, 3))
          board.move_piece(Coordinate.new(7, 4), Coordinate.new(7, 3))
          board.move_piece(Coordinate.new(0, 3), Coordinate.new(0, 4))
          board.move_piece(Coordinate.new(7, 3), Coordinate.new(7, 2))
          board.move_piece(Coordinate.new(0, 4), Coordinate.new(0, 5))
          board.move_piece(Coordinate.new(7, 2), Coordinate.new(6, 1))
          board.move_piece(Coordinate.new(0, 5), Coordinate.new(1, 6))
          board.move_piece(Coordinate.new(6, 1), Coordinate.new(5, 0))
          expect(board.pawn_ready_for_promotion?[0]).to be true
        end
      end
    end

    context 'when no pawn is ready for promotion' do
      it 'returns false' do
        expect(board.pawn_ready_for_promotion?[0]).to be false
      end
    end
  end

  describe '.promote_pawn' do
    context 'when a pawn is ready for promotion' do
      before(:each) do
        # move white pawn to end
        board.move_piece(Coordinate.new(0, 1), Coordinate.new(0, 3))
        board.move_piece(Coordinate.new(7, 6), Coordinate.new(7, 5))
        board.move_piece(Coordinate.new(0, 3), Coordinate.new(0, 4))
        board.move_piece(Coordinate.new(7, 5), Coordinate.new(7, 4))
        board.move_piece(Coordinate.new(0, 4), Coordinate.new(0, 5))
        board.move_piece(Coordinate.new(7, 4), Coordinate.new(7, 3))
        board.move_piece(Coordinate.new(0, 5), Coordinate.new(1, 6))
        board.move_piece(Coordinate.new(7, 3), Coordinate.new(7, 2))
        board.move_piece(Coordinate.new(1, 6), Coordinate.new(2, 7))
      end

      context 'and we want a bishop' do
        let(:promote_text) { 'bishop' }
        it 'promotes it to a bishop' do
          expect(board.promote_pawn(promote_text)).to be true
        end
      end

      context 'and we want a rook' do
        let(:promote_text) { 'rook' }
        it 'promotes it to a rook' do
          expect(board.promote_pawn(promote_text)).to be true
        end
      end

      context 'and we want a knight' do
        let(:promote_text) { 'knight' }
        it 'promotes it to a knight' do
          expect(board.promote_pawn(promote_text)).to be true
        end
      end

      context 'and we want a queen' do
        let(:promote_text) { 'queen' }
        it 'promotes it to a queen' do
          expect(board.promote_pawn(promote_text)).to be true
        end
      end
    end

    context 'when no pawn is ready for promotion' do
      let(:promote_text) { 'bishop' }
      it 'returns false' do
        expect(board.promote_pawn(promote_text)).to be false
      end
    end
  end

  describe '.move_piece' do
    context 'when piece is a white king' do
      context 'and is able to castle king side' do
        it 'castles' do
          board.move_piece(Coordinate.new(6, 1), Coordinate.new(6, 3))
          board.move_piece(Coordinate.new(6, 6), Coordinate.new(6, 4))
          board.move_piece(Coordinate.new(5, 1), Coordinate.new(5, 3))
          board.move_piece(Coordinate.new(5, 6), Coordinate.new(5, 4))
          board.move_piece(Coordinate.new(6, 0), Coordinate.new(5, 2))
          board.move_piece(Coordinate.new(6, 7), Coordinate.new(5, 5))
          board.move_piece(Coordinate.new(5, 0), Coordinate.new(7, 2))
          board.move_piece(Coordinate.new(5, 7), Coordinate.new(7, 5))

          board.move_piece(Coordinate.new(4, 0), Coordinate.new(6, 0))
          expect(board.board[0]).to eq([4, 2, 3, 5, 0, 4, 6, 0])
        end
      end
      context 'and is able to castle queen side' do
        it 'castles' do
          board.move_piece(Coordinate.new(2, 1), Coordinate.new(2, 3))
          board.move_piece(Coordinate.new(2, 6), Coordinate.new(2, 4))
          board.move_piece(Coordinate.new(1, 0), Coordinate.new(2, 2))
          board.move_piece(Coordinate.new(1, 7), Coordinate.new(2, 5))
          board.move_piece(Coordinate.new(3, 0), Coordinate.new(0, 3))
          board.move_piece(Coordinate.new(3, 7), Coordinate.new(0, 4))
          board.move_piece(Coordinate.new(1, 1), Coordinate.new(1, 3))
          board.move_piece(Coordinate.new(1, 6), Coordinate.new(1, 4))
          board.move_piece(Coordinate.new(2, 0), Coordinate.new(0, 2))
          board.move_piece(Coordinate.new(2, 7), Coordinate.new(0, 5))

          board.move_piece(Coordinate.new(4, 0), Coordinate.new(2, 0))
          expect(board.board[0]).to eq([0, 0, 6, 4, 0, 3, 2, 4])
        end
      end
    end

    context 'when a piece is a black king' do
      context 'and is able to castle king side' do
        it 'castles' do
          board.move_piece(Coordinate.new(6, 1), Coordinate.new(6, 3))
          board.move_piece(Coordinate.new(6, 6), Coordinate.new(6, 4))
          board.move_piece(Coordinate.new(5, 1), Coordinate.new(5, 3))
          board.move_piece(Coordinate.new(5, 6), Coordinate.new(5, 4))
          board.move_piece(Coordinate.new(6, 0), Coordinate.new(5, 2))
          board.move_piece(Coordinate.new(6, 7), Coordinate.new(5, 5))
          board.move_piece(Coordinate.new(5, 0), Coordinate.new(7, 2))
          board.move_piece(Coordinate.new(5, 7), Coordinate.new(7, 5))
          board.move_piece(Coordinate.new(4, 0), Coordinate.new(6, 0))

          board.move_piece(Coordinate.new(4, 7), Coordinate.new(6, 7))
          expect(board.board[7]).to eq([-4, -2, -3, -5, 0, -4, -6, 0])
        end
      end

      context 'and is able to castle queen side' do
        it 'castles' do
          board.move_piece(Coordinate.new(2, 1), Coordinate.new(2, 3))
          board.move_piece(Coordinate.new(2, 6), Coordinate.new(2, 4))
          board.move_piece(Coordinate.new(1, 0), Coordinate.new(2, 2))
          board.move_piece(Coordinate.new(1, 7), Coordinate.new(2, 5))
          board.move_piece(Coordinate.new(3, 0), Coordinate.new(0, 3))
          board.move_piece(Coordinate.new(3, 7), Coordinate.new(0, 4))
          board.move_piece(Coordinate.new(1, 1), Coordinate.new(1, 3))
          board.move_piece(Coordinate.new(1, 6), Coordinate.new(1, 4))
          board.move_piece(Coordinate.new(2, 0), Coordinate.new(0, 2))
          board.move_piece(Coordinate.new(2, 7), Coordinate.new(0, 5))
          board.move_piece(Coordinate.new(4, 0), Coordinate.new(2, 0))

          board.move_piece(Coordinate.new(4, 7), Coordinate.new(2, 7))
          expect(board.board[7]).to eq([0, 0, -6, -4, 0, -3, -2, -4])
        end
      end
    end

    context 'when piece is a pawn' do
      context 'and it can capture en passant' do
        it 'captures' do
          board.move_piece(Coordinate.new(1, 1), Coordinate.new(1, 3))
          board.move_piece(Coordinate.new(1, 6), Coordinate.new(1, 5))
          board.move_piece(Coordinate.new(1, 3), Coordinate.new(1, 4))
          board.move_piece(Coordinate.new(2, 6), Coordinate.new(2, 4))

          board.move_piece(Coordinate.new(1, 4), Coordinate.new(2, 5))
          expect(board.board).to eq([[4, 2, 3, 5, 6, 3, 2, 4], 
                                     [1, 0, 1, 1, 1, 1, 1, 1],
                                     [0, 0, 0, 0, 0, 0, 0, 0],
                                     [0, 0, 0, 0, 0, 0, 0, 0],
                                     [0, 0, 0, 0, 0, 0, 0, 0], 
                                     [0, -1, 1, 0, 0, 0, 0, 0],
                                     [-1, 0, 0, -1, -1, -1, -1, -1], 
                                     [-4, -2, -3, -5, -6, -3, -2, -4]])
        end
      end
    end
  end

  describe '.checkmate?' do
    context 'when white king' do
      context 'in checkmate' do
        it 'returns true' do
          # fool's mate
          board.move_piece(Coordinate.new(5, 1), Coordinate.new(5, 2))
          board.move_piece(Coordinate.new(4, 6), Coordinate.new(4, 4))
          board.move_piece(Coordinate.new(6, 1), Coordinate.new(6, 3))
          board.move_piece(Coordinate.new(3, 7), Coordinate.new(7, 3))
          expect(board.checkmate?).to be true
        end
      end
      context 'not in checkmate' do
        it 'returns false' do
          # fool's mate
          board.move_piece(Coordinate.new(5, 1), Coordinate.new(5, 2))
          board.move_piece(Coordinate.new(4, 6), Coordinate.new(4, 4))
          board.move_piece(Coordinate.new(6, 1), Coordinate.new(6, 3))
          board.move_piece(Coordinate.new(1, 1), Coordinate.new(1, 2))
          board.move_piece(Coordinate.new(5, 6), Coordinate.new(5, 5))
          board.move_piece(Coordinate.new(3, 7), Coordinate.new(7, 3))
          expect(board.checkmate?).to be false
        end
      end
    end

    context 'when black king' do
      context 'in checkmate' do
        it 'returns true' do
          # fool's mate
          board.move_piece(Coordinate.new(0, 1), Coordinate.new(0, 2)) # random move so white can start sequence
          board.move_piece(Coordinate.new(5, 6), Coordinate.new(5, 5))
          board.move_piece(Coordinate.new(4, 1), Coordinate.new(4, 3))
          board.move_piece(Coordinate.new(6, 6), Coordinate.new(6, 4))
          board.move_piece(Coordinate.new(3, 0), Coordinate.new(7, 4))
          expect(board.checkmate?).to be true
        end
      end
      context 'not in checkmate' do
        it 'returns false' do
          expect(board.checkmate?).to be false
        end
      end
    end
  end

  describe '.get_fen_string' do
    context 'when board in start position' do
      it 'returns fen string' do
        expect(board.get_fen_string).to eq('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1')
      end

      context 'and we made a move' do
        it 'returns fen string' do
          board.move_piece(Coordinate.new(4, 1), Coordinate.new(4, 3))
          expect(board.get_fen_string).to eq('rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1')
        end
      end

      context 'and we make 2 moves' do
        it 'returns fen string' do
          board.move_piece(Coordinate.new(4, 1), Coordinate.new(4, 3))
          board.move_piece(Coordinate.new(2, 6), Coordinate.new(2, 4))
          expect(board.get_fen_string).to eq('rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2')
        end
      end

      context 'and we make 3 moves' do
        it 'returns fen string' do
          board.move_piece(Coordinate.new(4, 1), Coordinate.new(4, 3))
          board.move_piece(Coordinate.new(2, 6), Coordinate.new(2, 4))
          board.move_piece(Coordinate.new(6, 0), Coordinate.new(5, 2))
          expect(board.get_fen_string).to eq('rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2')
        end
      end
    end
  end

  context 'when board in Kiwipete position' do
      it 'returns fen string' do
        expect(board.read_fen_string('r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0')).to eq(BoardState.new([[4, 0, 0, 0, 6, 0, 0, 4], [1, 1, 1, 3, 3, 1, 1, 1],
                                                                                                                                    [0, 0, 2, 0, 0, 5, 0, -1], [0, -1, 0, 0, 1, 0, 0, 0],
                                                                                                                                    [0, 0, 0, 1, 2, 0, 0, 0], [-3, -2, 0, 0, -1, -2, -1, 0],
                                                                                                                                    [-1, 0, -1, -1, -5, -1, -3, 0], [-4, 0, 0, 0, -6, 0, 0, -4]],
                                                                                                                                   1, 'KQkq', nil, 0, 0))
      end
      context 'and we make an en passant move' do
        it 'returns fen string' do
          board.read_fen_string('r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0')
          board.move_piece(Coordinate.new(0, 1), Coordinate.new(0, 3))
          board.move_piece(Coordinate.new(1, 3), Coordinate.new(0, 2))
          expect(board.get_fen_string).to eq('r3k2r/p1ppqpb1/bn2pnp1/3PN3/4P3/p1N2Q1p/1PPBBPPP/R3K2R w KQkq - 0 1')
        end
    end
  end

  context 'when board in pins position' do
    context 'and we make a move' do
      it 'returns fen string' do
        board.read_fen_string('r3k2r/1bp2pP1/5n2/1P1Q4/1pPq4/5N2/1B1P2p1/R3K2R b KQkq c3 0 1')
        board.move_piece(Coordinate.new(3, 3), Coordinate.new(4, 2))
        expect(board.get_fen_string).to eq('r3k2r/1bp2pP1/5n2/1P1Q4/1pP5/4qN2/1B1P2p1/R3K2R w KQkq - 1 2')
      end
    end
  end

  describe '.read_fen_string' do
    context 'when given a start position fen string' do
      it 'returns board state in start position' do
        expect(board.read_fen_string('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1')).to eq(BoardState.new([[4, 2, 3, 5, 6, 3, 2, 4], [1, 1, 1, 1, 1, 1, 1, 1],
                                                                                                                        [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0],
                                                                                                                        [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0],
                                                                                                                        [-1, -1, -1, -1, -1, -1, -1, -1], [-4, -2, -3, -5, -6, -3, -2, -4]],
                                                                                                                       1, 'KQkq', nil, 0, 1))
      end
    end
    context 'when we made a move from start position' do
      it 'returns board state' do
        board.move_piece(Coordinate.new(4, 1), Coordinate.new(4, 3))
        expect(board.read_fen_string(board.get_fen_string)).to eq(BoardState.new([[4, 2, 3, 5, 6, 3, 2, 4], [1, 1, 1, 1, 0, 1, 1, 1],
                                                                                  [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 1, 0, 0, 0],
                                                                                  [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0],
                                                                                  [-1, -1, -1, -1, -1, -1, -1, -1], [-4, -2, -3, -5, -6, -3, -2, -4]],
                                                                                 -1, 'KQkq', Coordinate.new(4, 2), 0, 1))
      end
    end
  end
end
