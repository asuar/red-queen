# frozen_string_literal: true

require './lib/perft.rb'
require './lib/utils.rb'

describe Perft do
  subject(:perft) { Perft.new }
  describe '.get_perft' do
    context 'standard start' do
      context 'at depth 1' do
        xit 'returns correct count' do
          expect(perft.get_perft(1)[0]).to eq(20)
        end
      end
      context 'at depth 2' do
        xit 'returns correct count' do
          expect(perft.get_perft(2)[0]).to eq(400)
        end
      end
      context 'at depth 3' do
        it 'returns correct count' do
          expect(perft.get_perft(3)[0]).to eq(8902)
        end
      end
      context 'at depth 4' do
        xit 'returns correct count' do
          expect(perft.get_perft(4)[0]).to eq(197_281)
        end
      end
      context 'at depth 5' do
        xit 'returns correct count' do
          expect(perft.get_perft(5)[0]).to eq(4_865_609)
        end
      end
      context 'at depth 6' do
        xit 'returns correct count' do
          expect(perft.get_perft(6)[0]).to eq(119_060_324)
        end
      end
      context 'at depth 7' do
        xit 'returns correct count' do
          expect(perft.get_perft(7)[0]).to eq(3_195_901_860)
        end
      end
    end

    context 'promotion start' do
      context 'at depth 1' do
        xit 'returns correct count' do
          expect(perft.get_perft(1, 'K6k/8/8/2P5/8/8/4p3/8 w - - 0 0')[0]).to eq(4)
        end
      end
      context 'at depth 2' do
        it 'returns correct count' do
          expect(perft.get_perft(2, 'K6k/8/8/2P5/8/8/4p3/8 w - - 0 0')[0]).to eq(28)
        end
      end
      context 'at depth 3' do
        it 'returns correct count' do
          expect(perft.get_perft(3, 'K6k/8/8/2P5/8/8/4p3/8 w - - 0 0')[0]).to eq(175)
        end
      end
      context 'at depth 4' do
        it 'returns correct count' do
          expect(perft.get_perft(4, 'K6k/8/8/2P5/8/8/4p3/8 w - - 0 0')[0]).to eq(2200)
        end
      end
      context 'at depth 5' do
        it 'returns correct count' do
          expect(perft.get_perft(5, 'K6k/8/8/2P5/8/8/4p3/8 w - - 0 0')[0]).to eq(13_478)
        end
      end
      context 'at depth 6' do
        it 'returns correct count' do
          expect(perft.get_perft(6, 'K6k/8/8/2P5/8/8/4p3/8 w - - 0 0')[0]).to eq(216_317)
        end
      end
    end

    # r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq -
    context 'Kiwipete start' do
      context 'at depth 1' do
        it 'returns correct count' do
          expect(perft.get_perft(1, 'r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0')[0]).to eq(48)
        end
      end
      context 'at depth 2' do
        it 'returns correct count' do
          expect(perft.get_perft(2, 'r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0')[0]).to eq(2039)
        end
      end
      context 'at depth 3' do
        it 'returns correct count' do
          expect(perft.get_perft(3, 'r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0')[0]).to eq(97_862)
        end
      end
      context 'at depth 4' do
        xit 'returns correct count' do
          expect(perft.get_perft(4, 'r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0')[0]).to eq(4_085_603)
        end
      end
      context 'at depth 5' do
        xit 'returns correct count' do
          expect(perft.get_perft(5, 'r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0')[0]).to eq(193_690_690)
        end
      end
      context 'at depth 6' do
        xit 'returns correct count' do
          expect(perft.get_perft(6, 'r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0')[0]).to eq(8_031_647_685)
        end
      end
    end

    # 8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - -
    context 'Position 3 https://www.chessprogramming.org/Perft_Results start' do
      context 'at depth 1' do
        xit 'returns correct count' do
          expect(perft.get_perft(1, '8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 0')[0]).to eq(14)
        end
      end
      context 'at depth 2' do
        xit 'returns correct count' do
          expect(perft.get_perft(2, '8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 0')[0]).to eq(191)
        end
      end
      context 'at depth 3' do
        xit 'returns correct count' do
          expect(perft.get_perft(3, '8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 0')[0]).to eq(2812)
        end
      end
      context 'at depth 4' do
        it 'returns correct count' do
          expect(perft.get_perft(4, '8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 0')[0]).to eq(43_238)
        end
      end
      context 'at depth 5' do
        xit 'returns correct count' do
          expect(perft.get_perft(5, '8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 0')[0]).to eq(674_624)
        end
      end
      context 'at depth 6' do
        xit 'returns correct count' do
          expect(perft.get_perft(6, '8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 0')[0]).to eq(11_030_083)
        end
      end
    end

    # r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1
    context 'Position 4 https://www.chessprogramming.org/Perft_Results start' do
      context 'at depth 1' do
        xit 'returns correct count' do
          expect(perft.get_perft(1, 'r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1')[0]).to eq(6)
        end
      end
      context 'at depth 2' do
        xit 'returns correct count' do
          expect(perft.get_perft(2, 'r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1')[0]).to eq(264)
        end
      end
      context 'at depth 3' do
        it 'returns correct count' do
          expect(perft.get_perft(3, 'r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1')[0]).to eq(9467)
        end
      end
      context 'at depth 4' do
        xit 'returns correct count' do
          expect(perft.get_perft(4, 'r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1')[0]).to eq(422_333)
        end
      end
      context 'at depth 5' do
        xit 'returns correct count' do
          expect(perft.get_perft(5, 'r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1')[0]).to eq(15_833_292)
        end
      end
      context 'at depth 6' do
        xit 'returns correct count' do
          expect(perft.get_perft(6, 'r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1')[0]).to eq(706_045_033)
        end
      end
    end

    # rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8
    context 'Position 5 https://www.chessprogramming.org/Perft_Results start' do
      context 'at depth 1' do
        xit 'returns correct count' do
          expect(perft.get_perft(1, 'rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8')[0]).to eq(44)
        end
      end
      context 'at depth 2' do
        xit 'returns correct count' do
          expect(perft.get_perft(2, 'rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8')[0]).to eq(1486)
        end
      end
      context 'at depth 3' do
        it 'returns correct count' do
          expect(perft.get_perft(3, 'rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8')[0]).to eq(62_379)
        end
      end
      context 'at depth 4' do
        xit 'returns correct count' do
          expect(perft.get_perft(4, 'rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8')[0]).to eq(2_103_487)
        end
      end
      context 'at depth 5' do
        xit 'returns correct count' do
          expect(perft.get_perft(5, 'rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8')[0]).to eq(89_941_194)
        end
      end
    end

        # r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10 
        context 'Position 6 https://www.chessprogramming.org/Perft_Results start' do
            context 'at depth 1' do
              xit 'returns correct count' do
                expect(perft.get_perft(1, 'r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10 ')[0]).to eq(46)
              end
            end
            context 'at depth 2' do
              xit 'returns correct count' do
                expect(perft.get_perft(2, 'r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10 ')[0]).to eq(2079)
              end
            end
            context 'at depth 3' do
              xit 'returns correct count' do
                expect(perft.get_perft(3, 'r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10 ')[0]).to eq(89890)
              end
            end
            context 'at depth 4' do
              xit 'returns correct count' do
                expect(perft.get_perft(4, 'r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10 ')[0]).to eq(3894594)
              end
            end
            context 'at depth 5' do
              xit 'returns correct count' do
                expect(perft.get_perft(5, 'r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10 ')[0]).to eq(164075551)
              end
            end
          end

          #r3k2r/1bp2pP1/5n2/1P1Q4/1pPq4/5N2/1B1P2p1/R3K2R b KQkq c3 0 1
          context 'Position with pins https://www.chessprogramming.org/Perft_Results start' do
            context 'at depth 1' do
              xit 'returns correct count' do
                expect(perft.get_perft(1, 'r3k2r/1bp2pP1/5n2/1P1Q4/1pPq4/5N2/1B1P2p1/R3K2R b KQkq c3 0 1')[0]).to eq(60)
              end
            end
            context 'at depth 2' do
              xit 'returns correct count' do
                expect(perft.get_perft(2, 'r3k2r/1bp2pP1/5n2/1P1Q4/1pPq4/5N2/1B1P2p1/R3K2R b KQkq c3 0 1')[0]).to eq(2608)
              end
            end
            context 'at depth 3' do
              it 'returns correct count' do
                expect(perft.get_perft(3, 'r3k2r/1bp2pP1/5n2/1P1Q4/1pPq4/5N2/1B1P2p1/R3K2R b KQkq c3 0 1')[0]).to eq(113_742)
              end
            end
            context 'at depth 4' do
              xit 'returns correct count' do
                expect(perft.get_perft(4, 'r3k2r/1bp2pP1/5n2/1P1Q4/1pPq4/5N2/1B1P2p1/R3K2R b KQkq c3 0 1')[0]).to eq(4_812_099)
              end
            end
            context 'at depth 5' do
              xit 'returns correct count' do
                expect(perft.get_perft(5, 'r3k2r/1bp2pP1/5n2/1P1Q4/1pPq4/5N2/1B1P2p1/R3K2R b KQkq c3 0 1')[0]).to eq(202_902_054)
              end
            end
          end
  end

  describe '.divide' do
    context 'standard start' do
      context 'at depth 1' do
        xit 'returns correct count' do
          perft.get_divide(1)[0]
        end
      end
      context 'at depth 2' do
        xit 'returns correct count' do
          perft.get_divide(2)[0]
        end
      end
      context 'at depth 3' do
        xit 'returns correct count' do
          perft.get_divide(3)[0]
        end
      end
      context 'at depth 4' do
        xit 'returns correct count' do
          perft.get_divide(4)[0]
        end
      end
    end

    # K6k/8/8/2P5/8/8/4p3/8 w - - 0 0
    context 'promotion start' do
      context 'at depth 1' do
        xit 'returns correct count' do
          perft.get_divide(1, 'K6k/8/8/2P5/8/8/4p3/8 w - - 0 0')[0]
        end
      end

      context 'at depth 2' do
        xit 'returns correct count' do
          perft.get_divide(2, 'K6k/8/8/2P5/8/8/4p3/8 w - - 0 0')[0]
        end
      end
      context 'at depth 5' do
        xit 'returns correct count' do
          perft.get_divide(5, 'K6k/8/8/2P5/8/8/4p3/8 w - - 0 0')[0]
        end
      end
    end

    # r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq -
    context 'Kiwipete start' do
      context 'at depth 1' do
        xit 'returns correct count' do
          perft.get_divide(1, 'r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0')[0]
        end
      end
      context 'at depth 2' do
        xit 'returns correct count' do
          perft.get_divide(2, 'r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0')[0]
        end
      end
      context 'at depth 3' do
       xit 'returns correct count' do
          perft.get_divide(1, 'r3k2r/p1ppqpb1/1n2pnp1/3PN3/1p2P3/2N2Q1p/PPPBbPPP/R2K3R w kq - 0 2')[0]
        end
      end
    end

    # 8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - -
    context 'Position 3 https://www.chessprogramming.org/Perft_Results start' do
      context 'at depth 1' do
        xit 'returns correct count' do
          perft.get_divide(1, '8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 0')[0]
        end
      end
      context 'at depth 4' do
        xit 'returns correct count' do
          perft.get_divide(1, ' 8/2p5/3p4/KP4kr/6R1/8/4P1P1/8 b - - 2 2')[0]
        end
      end

      # rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8
      context 'Position 4 https://www.chessprogramming.org/Perft_Results start' do
        context 'at depth 1' do
          xit 'returns correct count' do
            perft.get_divide(1, 'r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P1RPP/R2Q2K1 b kq - 0 1')[0]
          end
        end
        context 'at depth 2' do
          xit 'returns correct count' do
            perft.get_divide(2, 'r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1')[0]
          end
        end
      end

      # rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8
      context 'Position 5 https://www.chessprogramming.org/Perft_Results start' do
        context 'at depth 1' do
          xit 'returns correct count' do
            perft.get_divide(1, 'rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8')[0]
          end
        end
        context 'at depth 2' do
          xit 'returns correct count' do
            perft.get_divide(2, 'rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8')[0]
          end
        end
      end
    end
      #r3k2r/1bp2pP1/5n2/1P1Q4/1pPq4/5N2/1B1P2p1/R3K2R b KQkq c3 0 1
      context 'Position with pins https://www.chessprogramming.org/Perft_Results start' do
        context 'at depth 1' do
          xit 'returns correct count' do
            perft.get_divide(2, 'r6r/1bp1kpP1/5n2/1P1Q4/1pPq4/5N2/1B1P2p1/R3K2R w KQ - 1 2')[0]
          end
        end
        context 'at depth 2' do
          xit 'returns correct count' do
            perft.get_divide(2, 'r3k2r/1bp2pP1/5n2/1P1Q4/1pPq4/5N2/1B1P2p1/R3K2R b KQkq c3 0 1')[0]
          end
        end
        context 'at depth 3' do
         xit 'returns correct count' do
            perft.get_divide(3, 'r3k2r/1bp2pP1/5n2/1P1Q4/1pPq4/5N2/1B1P2p1/R3K2R b KQkq c3 0 1')[0]   
          end
        end
      end

  end
end
