# frozen_string_literal: true
require 'ruby-prof'

module Utils
    
    public

    WHITE = 1
    BLACK = -1
    
    BLACK_PAWN = -1
    BLACK_KNIGHT = -2
    BLACK_BISHOP = -3
    BLACK_ROOK = -4
    BLACK_QUEEN = -5
    BLACK_KING = -6
    
    WHITE_PAWN = 1
    WHITE_KNIGHT = 2
    WHITE_BISHOP = 3
    WHITE_ROOK = 4
    WHITE_QUEEN = 5
    WHITE_KING = 6
    
    EMPTY = 0
    BOARD_WIDTH = 8

    def self.get_piece_color(piece)
        return EMPTY if piece == EMPTY

        piece > EMPTY ? WHITE : BLACK
    end 

    #example usage:
    # Utils.profile_code(perft.method(:get_perft), 4)
    def self.profile_code(func, *args)
        profile = RubyProf::Profile.new
        profile.exclude_common_methods!
        # profile the code
        profile.start
        # code to profile 
        func_result = func.call(*args)
        result = profile.stop
        # print a flat profile to text
        default_options = { sort_method: :total_time, filter_by: :total_time }
        printer = RubyProf::FlatPrinter.new(result).print($stdout, default_options)
        #pass through result
        func_result
    end

end