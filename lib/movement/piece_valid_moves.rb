# frozen_string_literal: true

class PieceValidMoves
  require_relative '../coordinate.rb'
  require_relative '../utils.rb'
  require_relative 'piece_moves.rb'
 
  def initialize(board)
    @board = board
    @board_height = board.size
    @board_width = board[0].size
  end

  public

  def generate_pseudo_legal_moves(chosen_piece_location,en_passant_target, castling_allowed)
    all_moves = PieceMoves.generate_moves(chosen_piece_location, @board[chosen_piece_location.y][chosen_piece_location.x])
    return [] if all_moves.nil?
    all_moves = get_moves_in_bounds(all_moves)
    all_moves = add_castle_moves(chosen_piece_location, all_moves, castling_allowed)
    filled_positions = get_filled_positions(all_moves)
    all_moves = prune_blocked_moves(chosen_piece_location, all_moves, filled_positions)
    prune_pawn_capture_moves(chosen_piece_location, all_moves, en_passant_target)
  end

  private

  def get_moves_in_bounds(moves)
    moves_in_bounds = moves.select do |move|
      if move.x < @board_width && move.x > -1 && move.y < @board_height && move.y > -1
        true
      else
        false
      end
    end
    moves_in_bounds
  end

  def get_filled_positions(positions)
    positions.reject do |position|
      @board[position.y][position.x] == Utils::EMPTY
    end
  end

  def add_castle_moves(chosen_piece_location, move_set, castling_allowed)
    piece_type = @board[chosen_piece_location.y][chosen_piece_location.x]
    piece_col = chosen_piece_location.x
    if piece_type == Utils::WHITE_KING && chosen_piece_location.y == 0 && piece_col == 4
      if @board[0][0] == 4 && @board[0][1] == 0 && @board[0][2] == 0 && @board[0][3] == 0 && castling_allowed.include?('Q')
        move_set << Coordinate.new(2, 0) # white queen side castle
      end
      if @board[0][7] == 4 && @board[0][6] == 0 && @board[0][5] == 0 && piece_col == 4 && castling_allowed.include?('K')
        move_set << Coordinate.new(6, 0) # white king side castle
      end
    elsif piece_type == Utils::BLACK_KING && chosen_piece_location.y == 7
      if @board[7][0] == -4 && @board[7][1] == 0 && @board[7][2] == 0 && @board[7][3] == 0 && piece_col == 4 && castling_allowed.include?('q')
        move_set << Coordinate.new(2, 7) # black queen side castle
      end
      if @board[7][7] == -4 && @board[7][6] == 0 && @board[7][5] == 0 && piece_col== 4 && castling_allowed.include?('k')
        move_set << Coordinate.new(6, 7) # black king side castle
      end
    end
     move_set
  end

  def prune_blocked_moves(piece_location, move_set, filled_positions)
    current_piece = @board[piece_location.y][piece_location.x]
    piece_color = Utils.get_piece_color(current_piece)
    previous_size = 0 # if no moves were removed we are done
    until filled_positions.empty? || previous_size == filled_positions.length
      previous_size = filled_positions.length

      blocking_piece = filled_positions.shift
      slope_x =  blocking_piece.x - piece_location.x
      slope_y =  blocking_piece.y - piece_location.y

      moves_to_keep_proc = slope_x == 0 || slope_y == 0 ? get_straight_prune_proc(slope_x, slope_y, blocking_piece) : get_diagonal_prune_proc(slope_x, slope_y, blocking_piece)
      move_set = prune_moves_past_location(move_set, blocking_piece, moves_to_keep_proc)
      filled_positions = prune_moves_past_location(filled_positions, blocking_piece, moves_to_keep_proc)

      # re-add it if its not a same color piece
      blocking_piece_color = @board[blocking_piece.y][blocking_piece.x] > 0 ? Utils::WHITE_PAWN : Utils::BLACK_PAWN
      move_set << blocking_piece if blocking_piece_color != piece_color
    end
    move_set
  end

  def prune_moves_past_location(move_set, blocking_piece, proc)
    move_set.select do |move|
      proc.call(move, blocking_piece)
    end
  end

  def get_straight_prune_proc(slope_x, slope_y, blocking_piece)
    # left
    # right
    if slope_x > 0
      moves_to_keep_proc = proc { |move| move.x < blocking_piece.x || move.y != blocking_piece.y }
    elsif slope_x < 0
      moves_to_keep_proc = proc { |move| move.x > blocking_piece.x || move.y != blocking_piece.y }
    end
    # up
    # down
    if slope_y > 0
      moves_to_keep_proc = proc { |move| move.y < blocking_piece.y || move.x != blocking_piece.x }
    elsif slope_y < 0
      moves_to_keep_proc = proc { |move| move.y > blocking_piece.y || move.x != blocking_piece.x }
    end
    moves_to_keep_proc
  end

  def get_diagonal_prune_proc(slope_x, slope_y, blocking_piece)
    # up right
    # up left
    if slope_y > 0
      if slope_x > 0
        moves_to_keep_proc = proc { |move| !(move.x >= blocking_piece.x && move.y >= blocking_piece.y) }
      else
        moves_to_keep_proc = proc { |move| !(move.x <= blocking_piece.x && move.y >= blocking_piece.y) }
      end
    end
    # down right
    # down left
    if slope_y < 0
      if slope_x > 0
        moves_to_keep_proc = proc { |move| !(move.x >= blocking_piece.x && move.y <= blocking_piece.y) }
      else
        moves_to_keep_proc = proc { |move| !(move.x <= blocking_piece.x && move.y <= blocking_piece.y) }
        end
    end
    moves_to_keep_proc
  end

  def prune_pawn_capture_moves(chosen_piece_location, all_moves, en_passant_target)
    current_piece = @board[chosen_piece_location.y][chosen_piece_location.x]
    return all_moves if (current_piece != Utils::BLACK_PAWN) && (current_piece != Utils::WHITE_PAWN)
 
    piece_color = Utils.get_piece_color(current_piece)
    all_moves = all_moves.select do |move|
      move_piece = @board[move.y][move.x]
      move_color = Utils.get_piece_color(move_piece)
      if move.x > chosen_piece_location.x || move.x < chosen_piece_location.x
        if en_passant_target.nil?
          piece_color != move_color && move_color != 0 ? true : false
        else
          piece_color != move_color && (move_color != 0 || en_passant_target == move) ? true : false
        end
      elsif move_color != 0
        false
      else
        true
      end
    end

    all_moves
  end
end
