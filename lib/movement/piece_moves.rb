# frozen_string_literal: true

module PieceMoves
  require_relative '../coordinate.rb'

  @@WHITE_PAWN_STARTING_ROW = 1
  @@BLACK_PAWN_STARTING_ROW = 6

  public

  def self.generate_moves(position, piece)
    case piece.abs
    when 1
      generate_pawn_moves(position, piece)
    when 2
      generate_knight_moves(position)
    when 3
      generate_bishop_moves(position)
    when 4
      generate_rook_moves(position)
    when 5
      generate_queen_moves(position)
    when 6
      generate_king_moves(position)
    end
  end

  private

  def self.generate_pawn_moves(position, piece_color)
    starting_position = piece_color > 0 ? @@WHITE_PAWN_STARTING_ROW : @@BLACK_PAWN_STARTING_ROW
    # 1 up or down based on color
    # 2 up or down if it hasnt moved before
    results = []
    results << Coordinate.new(position.x, position.y + (1 * piece_color))
    if position.y == starting_position
      results << Coordinate.new(position.x, position.y + (2 * piece_color))
    end
    # diagonals for capturing
    results << Coordinate.new(position.x + 1, position.y + (1 * piece_color))
    results << Coordinate.new(position.x - 1, position.y + (1 * piece_color))

    results
  end

  def self.generate_knight_moves(position)
    # example [0,0]
    results = []
    results << Coordinate.new(position.x + 1, position.y + 2)
    results << Coordinate.new(position.x + 2, position.y + 1)
    results << Coordinate.new(position.x + 2, position.y - 1)
    results << Coordinate.new(position.x + 1, position.y - 2)
    results << Coordinate.new(position.x - 1, position.y - 2)
    results << Coordinate.new(position.x - 2, position.y - 1)
    results << Coordinate.new(position.x - 2, position.y + 1)
    results << Coordinate.new(position.x - 1, position.y + 2)
    results
  end

  def self.generate_bishop_moves(position)
    # at most 7 to its diagonally -> right-up, left-up, right-down, left-down
    results = []
    (1..7).each do |index|
      results << Coordinate.new(position.x + index, position.y + index)
    end

    (1..7).each do |index|
      results << Coordinate.new(position.x - index, position.y + index)
    end

    (1..7).each do |index|
      results << Coordinate.new(position.x + index, position.y - index)
    end

    (1..7).each do |index|
      results << Coordinate.new(position.x - index, position.y - index)
    end

    results
  end

  def self.generate_rook_moves(position)
    # at most 7 to its left,right, up or down
    results = []
    (1..7).each do |index|
      results << Coordinate.new(position.x, position.y + index)
    end

    (1..7).each do |index|
      results << Coordinate.new(position.x, position.y - index)
    end

    (1..7).each do |index|
      results << Coordinate.new(position.x + index, position.y)
    end

    (1..7).each do |index|
      results << Coordinate.new(position.x - index, position.y)
    end

    results
  end

  def self.generate_queen_moves(position)
    # at most 7 to its diagonally -> right-up, left-up, right-down, left-down
    # at most 7 to its left,right, up or down
    results = generate_rook_moves(position)
    results += generate_bishop_moves(position)
  end

  def self.generate_king_moves(position)
    # 1 in any direction
    results = []
    results << Coordinate.new(position.x, position.y + 1)
    results << Coordinate.new(position.x, position.y - 1)
    results << Coordinate.new(position.x + 1, position.y)
    results << Coordinate.new(position.x - 1, position.y)
    results << Coordinate.new(position.x + 1, position.y + 1)
    results << Coordinate.new(position.x - 1, position.y + 1)
    results << Coordinate.new(position.x + 1, position.y - 1)
    results << Coordinate.new(position.x - 1, position.y - 1)
    # castling handled in game logic
    results
  end
end
