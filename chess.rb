# frozen_string_literal: true

class Chess
  require_relative 'lib/board.rb'
  require_relative 'lib/coordinate.rb'
  require_relative 'lib/game/player.rb'
  require_relative 'lib/game/game_serializer.rb'


  @@LETTER_START_ASCII = 65
  @@NUMBER_START_ASCII = 48

  def initialize
    @board = Board.new
    @save_manager = GameSerializer.new()
  end

  public

  def play_game
    start_game
    game_loop
    end_game
  end

  private

  def start_game
    display_starting_screen

    loop do
      puts "New game? ('Y' or 'N')"
      new_game_input = gets.chomp.downcase
      if new_game_input == 'y'
        create_players
        return
      elsif new_game_input == 'n'        
        return if load_save_file
      end
    end
  end

  def game_loop
    until game_over? do
      puts "Turn #{@board.fullmove_count}"
      display_board
      play_move
    end
  end

  def game_over?
    @board.checkmate? || @board.draw?
  end

  def end_game
    declare_winner
    if replay_game?
      play_game
    else
      puts "Thanks for playing!"
      exit
    end
  end

  def display_starting_screen
    puts '*******************************'
    puts '*    Chess by Alain Suarez    *'
    puts '*******************************'
    puts '* One player plays as white   *'
    puts '* and the other as black. The *'
    puts '* players take turns moving   *'
    puts '* pieces to capture the       *'
    puts '* opposing players king piece.*'
    puts '* White goes first.           *'
    puts '*******************************'
  end

  def display_board
    visual_board = @board.board
    puts '     A    B    C    D    E    F    G    H'
    puts '  -----------------------------------------'
    visual_board.reverse.each_with_index do |row, r_index|
      print "#{8 - r_index} |"
      row.each_with_index do |position, _c_index|
        print " #{print_piece(position)}  |"
      end
      puts " #{8 - r_index}"
      puts '  -----------------------------------------'
    end
    puts '     A    B    C    D    E    F    G    H'
  end

  def create_players
    @player1 = create_player(1)
    @player2 = create_player(-1)
  end

  def print_piece(piece_number)
    case piece_number
    when Utils::EMPTY
      ' '
    when Utils::BLACK_PAWN
      "\u2659".encode('utf-8')
    when Utils::BLACK_KNIGHT
      "\u2658".encode('utf-8')
    when Utils::BLACK_BISHOP
      "\u2657".encode('utf-8')
    when Utils::BLACK_ROOK
      "\u2656".encode('utf-8')
    when Utils::BLACK_QUEEN
      "\u2655".encode('utf-8')
    when Utils::BLACK_KING
      "\u2654".encode('utf-8')
    when Utils::WHITE_PAWN
      "\u265F".encode('utf-8')
    when Utils::WHITE_KNIGHT
      "\u265E".encode('utf-8')
    when Utils::WHITE_BISHOP
      "\u265D".encode('utf-8')
    when Utils::WHITE_ROOK
      "\u265C".encode('utf-8')
    when Utils::WHITE_QUEEN
      "\u265B".encode('utf-8')
    when Utils::WHITE_KING
      "\u265A".encode('utf-8')
    end
  end

  def create_player(color)
    colortext = color == 1 ? 'white' : 'black'
    puts "Who will play color #{colortext}?"
    player = Player.new(gets.chomp.capitalize, color)
  end

  def play_move
    current_player_to_move = @board.turn_to_move
    color_text = current_player_to_move == 1 ? 'white' : 'black'
    player_text = current_player_to_move == 1 ? @player1 : @player2

    piece_to_move = get_piece_to_move(color_text, player_text)
    piece_destination = get_piece_destination

    move_piece(piece_to_move, piece_destination)

    promote_piece(color_text, player_text) if @board.pawn_ready_for_promotion?[0]

  end

  def get_piece_to_move(color, player)
    loop do
      puts "Save the game by typing 'save'"
      puts "What piece do you want to move player #{color}(#{player})?"
      user_input = gets.chomp.capitalize

      if user_input == "Save"
        save_game
      end

      if user_input.match(/[A-H][1-8]$/)
        return convert_to_board_position(user_input)
      end
    end
  end

  def get_piece_destination
    loop do
      puts 'Where do you want to move it?'
      destination = gets.chomp.capitalize

      if destination.match(/[A-H][1-8]$/)
        return convert_to_board_position(destination)
      end
    end
  end

  def move_piece(piece_to_move, piece_destination)
    piece_to_move = Coordinate.new(piece_to_move[0], piece_to_move[1])
    piece_destination = Coordinate.new(piece_destination[0], piece_destination[1])

    result = @board.move_piece(piece_to_move, piece_destination)
    unless result
      invalid_reason = @board.invalid_move_reason(piece_to_move, piece_destination)
      puts "Invalid move: #{invalid_reason}"
    end
  end

  def convert_to_board_position(position)
    coordinates = position.bytes.to_a
    coordinates[0] = coordinates[0] % @@LETTER_START_ASCII
    coordinates[1] = (coordinates[1] % @@NUMBER_START_ASCII) - 1
    coordinates
  end

  def promote_piece(color, player)
    loop do
      puts "What piece do you want to promote your pawn to player #{color}(#{player})?"
      puts "(Enter 'bishop','knight','rook' or 'queen')"
      chosen_promotion = gets.chomp.downcase

      if chosen_promotion.match(/^bishop$|^rook$|^knight$|^queen$/)
        return @board.promote_pawn(chosen_promotion)
      end
    end
  end

  def load_save_file
    save_state = @save_manager.load_game()
    return false if save_state.nil?
    @player1 = save_state.white_player
    @player2 = save_state.black_player
    @board.read_fen_string(save_state.position_fen_string)
    true
  end

  def save_game
    current_game_state = GameState.new(@player1,@player2,@board.get_fen_string)
    @save_manager.save_game(current_game_state)
    puts "Bye!"
    exit
  end

  def declare_winner
    reason = @board.game_over_reason
    puts "Game over! The game ended in #{reason}!"
  end

  def replay_game?
    loop do
      puts "Rematch? ('Y' or 'N')"
      rematch_input = gets.chomp.downcase
      if rematch_input == 'y'
        @board = Board.new
        return true
      elsif rematch_input == 'n'

        return false
      end
    end
  end
end

chess = Chess.new
chess.play_game
